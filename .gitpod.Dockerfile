
FROM registry.gitlab.com/tezos/opam-repository:runtime-build-dependencies--b53a5e8c3fa7764d5ae6567d7a817315507c9f0d

RUN git clone --branch latest-release https://gitlab.com/tezos/tezos.git
RUN cd tezos && eval $(opam env) && make build-deps && eval $(opam env) && make
RUN sudo mv tezos/tezos-* /usr/bin

# gitpodify
RUN sudo addgroup -g 33333 gitpod
RUN sudo adduser -h /home/gitpod -s /bin/sh -D -G gitpod -u 33333 gitpod