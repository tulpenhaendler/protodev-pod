# Protodev Pod

This is a gitpod.io workspace for working on tezos protocols, it makes it very easy to get started
by providing a few helper scripts, a VSCode-like IDE with relevant plugins preinstalled,
as well as all Ocaml/Rust dependencies needed for compiling tezos preinstalled.

To build a custom protocol, follow these steps:

1. make sure you have a gitpod.io account ( its free, login with gitlab )
2. [click this link to open this workspace in gitpod](https://gitpod.io/#https://gitlab.com/tulpenhaendler/protodev-pod)
3. clone the tezos repository

```
git clone --branch latest-release https://gitlab.com/tezos/tezos.git
```
4. change the relevant files in tezos/src/proto_alpha
5. [prepare the protocol release](https://todo.example.com)
6. build the tezos node
7. [test your new protocol](https://todo.example.com)


We are open to MR to improve this repository, *especially Documentation*

## Related Documentation and Resources

- [tezos docs](https://tezos.gitlab.io/)
- [tezos docs: protocol release checklist](https://tezos.gitlab.io/developer/protocol_release_checklist.html?highlight=checklist)
- [tezos docs: sandbox](https://tezos.gitlab.io/user/sandbox.html)
- [tezosagora wiki](https://wiki.tezosagora.org/)
- [How to write a Tezos protocol - Blogpost by Nomadic Labs](https://research-development.nomadic-labs.com/how-to-write-a-tezos-protocol-part-2.html)